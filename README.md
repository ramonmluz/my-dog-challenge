# Desafio Idwall
Projeto desafio para listar raças de cachorros através de uma busca na API. 
Para lista essas raças é necessário informar um e-mail na tela inicial que ficará armazenado localmente.

Linguagem utilizada
-------------
- **Kotlin**

Arquitetura que será utilizada
-------------

- **MVVM com Live Data e DataBind:**  Foi utilizada esta arquitetura e componentes, visando: estruturar código em camadas de apresentação, negócio e acesso dados; manter os dados os dados durante as mudanças de cnfiguração; subtrair um pouco da responsabilidade de tela; manter o projeto com menor acoplamento e mais coesão.   
----------

Bibliotecas de Terceiros a serem utilizadas
-------------
- **Koin** 
- **Picasso** 
- **Coroutines** 
- **Gson** 
- **OkHttp** 
- **Navigation**
- **Firebase - Distribuição da Apk**

Biblioteca para acesso a Api
-------------
- **Retrofit**

Bibliotecas para Testes
-------------
- **Mockito,Junit - Teste unitário**


Status do projeto
-------------
- **Foram feitos alguns testes unitários**

- **Corrigido problema no back button**

- **Não foram feitos testes instrumentados**

Implementações futuras
-------------
- **Visando uma futura modularização, poderia ser criado um módulo core(classes java/koltin) para ser implementado na camada voltada para apresentação(Acitivity, fragment, VewModels)**
- **Podeira ser cirado uma camada interactor entre a viewModel e o Repository **
- **Usar criptografia para salvar o token**
- ** Usar o Espresso para teste de interação**








