package co.idwall.mydogapp.common.binding

import android.widget.EditText
import androidx.databinding.BindingAdapter
import co.idwall.mydogapp.R
import co.idwall.mydogapp.common.EmailValidator
import com.google.android.material.textfield.TextInputLayout


object EmailBinding {
    @JvmStatic
    @BindingAdapter("app:errorText")
    fun setErrorMessage(view: TextInputLayout, errorMessage: String?) {
        if(EmailValidator.isValidEmail(errorMessage?:"")){
            view.error = null
        }else{
            view.error = view.context.getString(R.string.email_invalid_informed)
        }
    }
}