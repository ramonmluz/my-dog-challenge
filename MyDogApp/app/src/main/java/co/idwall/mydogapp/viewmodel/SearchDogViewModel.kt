package co.idwall.mydogapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.idwall.mydogapp.model.DogCategory
import co.idwall.mydogapp.model.User
import co.idwall.mydogapp.network.StatusLoading
import co.idwall.mydogapp.repository.SearchDogRepository
import kotlinx.coroutines.launch

class SearchDogViewModel(private val searchDogRepository: SearchDogRepository) : ViewModel() {

    private val dogCategoryResult = MutableLiveData<StatusLoading<DogCategory>>()

    fun getDogCategoryResult(): LiveData<StatusLoading<DogCategory>> = dogCategoryResult

    fun searchDogs(token:String, category: String) {
        dogCategoryResult.postValue(StatusLoading.Loading(null))
        viewModelScope.launch {
            try {
                val dogCategory = searchDogRepository.searchDogs(token, category)
                resultSuccess(dogCategory)
            } catch (e: Exception) {
                resultError(e)
            }
        }
    }

    private fun resultSuccess(dogCategory: DogCategory) {
        dogCategoryResult.postValue(StatusLoading.Success(dogCategory))
    }

    private fun resultError(e: Exception) {
        dogCategoryResult.postValue(StatusLoading.Error(e.message))
    }
}