package co.idwall.mydogapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.idwall.mydogapp.R
import com.squareup.picasso.Picasso


class SearchDogViewAdapter(
    private val dogs: List<String>?,
    private val callback: SearchDogRecyclerViewItemDelegate
) : RecyclerView.Adapter<SearchDogViewHolder>() {

    var category: String = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchDogViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_seach_dog_item, parent, false)
        val viewHolder = SearchDogViewHolder(view)

        viewHolder.dogImageGrid.setOnClickListener {
            this.callback.onTouchImageItem(viewHolder.dogImageGrid.tag as Pair<String, String>)
        }

        return viewHolder
    }

    override fun getItemCount(): Int {
        return dogs?.size ?: 0
    }

    override fun onBindViewHolder(holder: SearchDogViewHolder, position: Int) {
        dogs?.let {
            val urlImage = it[position]
            Picasso.get()
                .load(urlImage)
                .placeholder(R.drawable.ic_insert_photo_black_24dp)
                .error(R.drawable.ic_broken_image_black_24dp)
                .into(holder.dogImageGrid)

            holder.dogImageGrid.tag = Pair(category, urlImage)
        }
    }
}