package co.idwall.mydogapp.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.view_choose_dog_category_item.view.*
import kotlinx.android.synthetic.main.view_seach_dog_item.view.*

class ChooseDogCategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val dogCategory  = itemView.dogCategoryTxt
}