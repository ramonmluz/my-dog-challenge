package co.idwall.mydogapp.network

import co.idwall.mydogapp.model.DogCategory
import co.idwall.mydogapp.model.UserResponse
import retrofit2.http.*


interface DogCategoryServiceApi {
    @POST("signup")
    suspend fun getUser(@Body   params: HashMap<String, String>): UserResponse

    @GET("feed")
    suspend fun getDogs(
        @Header("Authorization") token: String,
        @Query("category") category: String
    ):DogCategory
}