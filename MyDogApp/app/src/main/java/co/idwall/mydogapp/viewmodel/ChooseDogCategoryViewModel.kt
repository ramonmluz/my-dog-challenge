package co.idwall.mydogapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.idwall.mydogapp.model.DogCategory
import co.idwall.mydogapp.network.StatusLoading
import co.idwall.mydogapp.repository.ChooseDogCategoryRepository
import kotlinx.coroutines.launch

class ChooseDogCategoryViewModel(private val chooseDogCategoryRepository: ChooseDogCategoryRepository) : ViewModel() {

    var categoriesResult = MutableLiveData<StatusLoading<List<String>>>()

    fun getCategoriesResult(): LiveData<StatusLoading<List<String>>> = categoriesResult

    fun loadCategories() {
        categoriesResult.postValue(StatusLoading.Loading(null))
        viewModelScope.launch {
            try {
                val dogCategories = chooseDogCategoryRepository.loadCategories()
                resultSuccess(dogCategories)
            } catch (e: Exception) {
                resultError(e)
            }
        }
    }

    private fun resultSuccess(dogCategories: List<String>) {
        categoriesResult.postValue(StatusLoading.Success(dogCategories))
    }

    private fun resultError(e: Exception) {
        categoriesResult.postValue(StatusLoading.Error(e.message))
    }
}