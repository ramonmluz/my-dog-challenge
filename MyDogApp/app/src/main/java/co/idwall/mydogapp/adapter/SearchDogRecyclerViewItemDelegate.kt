package co.idwall.mydogapp.adapter

interface SearchDogRecyclerViewItemDelegate {
    fun onTouchImageItem(params: Pair<String,String> )
}