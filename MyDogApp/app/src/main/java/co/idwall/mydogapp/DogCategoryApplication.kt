package co.idwall.mydogapp

import android.app.Application
import co.idwall.mydogapp.modules.Modules
import co.idwall.mydogapp.modules.NetworkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class DogCategoryApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@DogCategoryApplication)
            modules(
                listOf(
                    NetworkModule.netWorkModule,
                    Modules.loginModule,
                    Modules.searchDogModule,
                    Modules.chooseDogCategoryModule
                )
            )
        }
    }

}