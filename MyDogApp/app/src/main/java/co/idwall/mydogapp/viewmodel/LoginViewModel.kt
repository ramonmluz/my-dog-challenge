package co.idwall.mydogapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.idwall.mydogapp.model.User
import co.idwall.mydogapp.network.StatusLoading
import co.idwall.mydogapp.repository.LoginRepository
import kotlinx.coroutines.launch

class LoginViewModel(private val loginRepository: LoginRepository) : ViewModel() {

    private val userResult = MutableLiveData<StatusLoading<User>>()

    private val tokenResult = MutableLiveData<StatusLoading<String>>()

    val email = MutableLiveData<String>("")


    fun getUserResult(): LiveData<StatusLoading<User>> = userResult

    fun signUp() {
        userResult.postValue(StatusLoading.Loading(null))
        viewModelScope.launch {
            try {
                val requestUser = loginRepository.loadUser(email.value.toString())
                resultSuccess(requestUser.user)
            } catch (e: Exception) {
                resultError(e)
            }
        }
    }

    private fun resultSuccess(user: User) {
        userResult.postValue(StatusLoading.Success(user))
    }

    private fun resultError(e: Exception) {
        userResult.postValue(StatusLoading.Error(e.message))
    }

    fun getTokenResult(): LiveData<StatusLoading<String>> = tokenResult

    fun findToken() {
        tokenResult.postValue(StatusLoading.Loading(null))
        viewModelScope.launch {
            try {
                val token = loginRepository.getTokenAtPreference()
                tokenResultSuccess(token)
            } catch (e: Exception) {
                tokenResultError(e)
            }
        }
    }

    private fun tokenResultSuccess(token: String?) {
        tokenResult.postValue(StatusLoading.Success(token))
    }

    private fun tokenResultError(e: Exception) {
        tokenResult.postValue(StatusLoading.Error(e.message))
    }


}