package co.idwall.mydogapp.ui

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import co.idwall.mydogapp.R
import co.idwall.mydogapp.databinding.ActivityLoginBinding
import co.idwall.mydogapp.model.User
import co.idwall.mydogapp.network.StatusLoading
import co.idwall.mydogapp.viewmodel.LoginViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {

    private val viewModel: LoginViewModel by viewModel()
    private val binding: ActivityLoginBinding by lazy {
         DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDataBind()
        onObserverTokenResult()
        onObserverUserResult()
        findToken()
    }

    private fun initDataBind() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
    }

    private fun onObserverTokenResult() {
        viewModel.getTokenResult().observe(this, Observer { statusLoading ->
            when (statusLoading) {
                is StatusLoading.Loading -> setLoadingIndicator(true)
                is StatusLoading.Success -> onTokenSuccess(statusLoading.data)
                is StatusLoading.Error -> onTokenError(statusLoading.message)
            }
        })
    }

    private fun onTokenSuccess(token: String?) {
        if (!token.isNullOrEmpty()) {
            finish()
            navigateToHome(token)
        } else {
            setLoadingIndicator(false)
        }
    }

    private fun onTokenError(message: String?) {
        setLoadingIndicator(false)
        showMessage(message)
    }

    private fun onObserverUserResult() {
        viewModel.getUserResult().observe(this, Observer { statusLoading ->
            when (statusLoading) {
                is StatusLoading.Loading -> setLoadingIndicator(true)
                is StatusLoading.Success -> onSuccess(statusLoading.data as User)
                is StatusLoading.Error -> onError(statusLoading.message)
            }
        })
    }

    private fun onSuccess(user: User) {
        setLoadingIndicator(false)
        startActivity(Intent(this, MainActivity::class.java))
        navigateToHome(user.token)
    }

    private fun navigateToHome(token: String?) {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra(getString(R.string.token), token)
        startActivity(intent)
        finish()
    }

    private fun onError(message: String?) {
        setLoadingIndicator(false)
        showMessage(getString(R.string.error_message_default))
    }

    private fun showMessage(message: String?) {
        AlertDialog.Builder(this)
            .setTitle(R.string.app_name)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok, null)
            .show()
    }

    private fun setLoadingIndicator(isLoading: Boolean) {
        binding.isLoading = isLoading
    }

    private fun findToken() {
        setLoadingIndicator(true)
        viewModel.findToken()
    }
}
