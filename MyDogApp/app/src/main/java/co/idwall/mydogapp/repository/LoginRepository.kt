package co.idwall.mydogapp.repository

import co.idwall.mydogapp.common.PreferencesProvider
import co.idwall.mydogapp.model.UserResponse
import co.idwall.mydogapp.network.DogCategoryServiceApi

class LoginRepository(private val dogCategoryServiceApi: DogCategoryServiceApi, private val preferencesProvider: PreferencesProvider) {
    suspend fun loadUser(email: String): UserResponse {
        val responseUser = dogCategoryServiceApi.getUser(hashMapOf((Pair("email", email))))
        saveTokenAtPreference(token = responseUser.user.token)
        return responseUser
    }

    private fun saveTokenAtPreference(token: String) {
        preferencesProvider.saveAtPreference(token)
    }

    fun getTokenAtPreference(): String? {
        return preferencesProvider.getValueAtPreference()
    }
}