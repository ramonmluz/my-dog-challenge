package co.idwall.mydogapp.network

sealed class StatusLoading<T> (val data: T? = null, val message: String? = null) {
    class Success<T>(data: T?) : StatusLoading<T>(data)
    class Loading<T>(data: T?) : StatusLoading<T>(data)
    class Error<T>(message: String?, data: T? = null) : StatusLoading<T>(data, message)
}