package co.idwall.mydogapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserResponse (
    val user: User
): Parcelable