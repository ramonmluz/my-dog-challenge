package co.idwall.mydogapp.ui

import android.graphics.Rect
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.idwall.mydogapp.R
import co.idwall.mydogapp.adapter.SearchDogRecyclerViewItemDelegate
import co.idwall.mydogapp.adapter.SearchDogViewAdapter
import co.idwall.mydogapp.databinding.FragmentSearchDogBinding
import co.idwall.mydogapp.model.DogCategory
import co.idwall.mydogapp.network.StatusLoading
import co.idwall.mydogapp.viewmodel.SearchDogViewModel
import kotlinx.android.synthetic.main.fragment_search_dog.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class SearchDogFragment : Fragment(), SearchDogRecyclerViewItemDelegate {

    private val searchDogViewModel: SearchDogViewModel by viewModel()
    private val args by navArgs<SearchDogFragmentArgs>()
    private lateinit var binding: FragmentSearchDogBinding
    private lateinit var adapter: SearchDogViewAdapter
    private var dogs = mutableListOf<String>()
    private  lateinit var token: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_dog, container, false)
        binding.viewModel = searchDogViewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initRecyclerView()
        onObserverDogCategoryResult()
        getTokenByIntent()
        searchDogs()
        setupAreaErrorClick()
    }

    private fun initRecyclerView() {
        adapter = SearchDogViewAdapter(dogs, this)
        binding.apply {
            searchDogRecyclerView.layoutManager = GridLayoutManager(activity, 2)
            searchDogRecyclerView.adapter = adapter
            searchDogRecyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                    defineMarginsRecyclerView(parent, view, outRect)

                }
            })
        }
    }

    private fun defineMarginsRecyclerView(parent: RecyclerView, view: View, outRect: Rect) {
        val moviePosition: Int = parent.getChildLayoutPosition(view)

        if (moviePosition == 0 || moviePosition == 1) {
            outRect.top = getAnIntDp(8)
            defineMarginBottom(outRect)
        } else {
            defineMarginBottom(outRect)
        }

        if (moviePosition % 2 == 0) {
            defineMargin(outRect, 8, 4)
        } else {
            defineMargin(outRect, 4, 8)
        }
    }

    private fun defineMarginBottom(rect: Rect) {
        rect.bottom = getAnIntDp(8)
    }

    private fun defineMargin(rect: Rect, marginLeft: Int, marginRight: Int) {
        rect.left = getAnIntDp(marginLeft)
        rect.right = getAnIntDp(marginRight)
    }

    private fun getAnIntDp(value: Int): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value.toFloat(), resources.displayMetrics).toInt()
    }

    private fun onObserverDogCategoryResult() {
        searchDogViewModel.getDogCategoryResult().observe(viewLifecycleOwner, Observer { statusLoading ->
            when (statusLoading) {
                is StatusLoading.Loading -> setVisibilityComponentsIndicator(isLoading =  true, isAreaError = false)
                is StatusLoading.Success -> onSuccess(statusLoading.data as DogCategory)
                is StatusLoading.Error -> setVisibilityComponentsIndicator(isLoading =  false, isAreaError = true)
            }
        })
    }

    private fun getTokenByIntent() {
        token = activity?.intent?.getStringExtra(getString(R.string.token))?:""
    }

    private fun searchDogs() {
        searchDogViewModel.searchDogs(token, args.category)
    }

    private fun setupAreaErrorClick() {
      binding.contentError.areaError.setOnClickListener {
            searchDogs()
        }
    }

    private fun onSuccess(dogCategory: DogCategory) {
        setVisibilityComponentsIndicator(isLoading = false, isAreaError = false)
        dogs.addAll(dogCategory.list)
        adapter.category = dogCategory.category
        adapter.notifyItemInserted(dogs.lastIndex)
    }

    private fun setVisibilityComponentsIndicator(isLoading: Boolean, isAreaError: Boolean ) {
        binding.isLoading = isLoading
        binding.isAreaError = isAreaError
        binding.contentError.isAreaError = isAreaError
    }

    override fun onTouchImageItem(params: Pair<String, String>) {

        val action =  SearchDogFragmentDirections.actionNavigationHomeToDogImageFragment(params.first, params.second)
        findNavController().navigate(action)
    }

}