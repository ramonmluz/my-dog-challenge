package co.idwall.mydogapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.idwall.mydogapp.R
import com.squareup.picasso.Picasso


class ChooseDogCategoryAdapter(
    private val categories: List<String>?,
    private val callback: ChooseDogCategoryItemDelegate
) : RecyclerView.Adapter<ChooseDogCategoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChooseDogCategoryViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_choose_dog_category_item, parent, false)
        val viewHolder = ChooseDogCategoryViewHolder(view)

        viewHolder.dogCategory.setOnClickListener {
            this.callback.onTouchItem(viewHolder.dogCategory.text.toString())
        }

        return viewHolder
    }

    override fun getItemCount(): Int {
        return categories?.size ?: 0
    }

    override fun onBindViewHolder(holder: ChooseDogCategoryViewHolder, position: Int) {
        categories?.let {
            holder.dogCategory.text = it[position]
        }
    }
}