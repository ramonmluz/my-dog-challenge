package co.idwall.mydogapp.common

import android.content.Context
import android.content.SharedPreferences
import co.idwall.mydogapp.R

class PreferencesProvider(private val context: Context) {

    private val preference: SharedPreferences
        get() = context.getSharedPreferences(context.getString(R.string.settings), Context.MODE_PRIVATE)

    fun getValueAtPreference(): String? {
        return preference.getString(context.getString(R.string.token), null)
    }

    fun saveAtPreference(value: String) {
        preference.edit().apply(){
            putString(context.getString(R.string.token), value)
            commit()
        }
    }
}