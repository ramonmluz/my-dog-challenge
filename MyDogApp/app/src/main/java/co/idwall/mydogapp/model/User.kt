package co.idwall.mydogapp.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
     val email: String,
     val token: String
) : Parcelable