package co.idwall.mydogapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class DogCategory (
    val category: String,
    val list: List<String>
):Parcelable