package co.idwall.mydogapp.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.view_seach_dog_item.view.*

class SearchDogViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val dogImageGrid  = itemView.dogImageGrid
}