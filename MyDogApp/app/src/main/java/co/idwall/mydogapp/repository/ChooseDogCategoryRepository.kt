package co.idwall.mydogapp.repository

import co.idwall.mydogapp.network.DogCategoryServiceApi

class ChooseDogCategoryRepository(private val dogCategoryServiceApi: DogCategoryServiceApi) {

    var categories = mutableListOf("husky", "hound", "pug", "labrador")

    suspend fun loadCategories(): List<String>{
        return this.categories
    }
}
