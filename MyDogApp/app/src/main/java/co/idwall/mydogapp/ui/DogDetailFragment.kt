package co.idwall.mydogapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import co.idwall.mydogapp.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_dog_detail.*


class DogDetailFragment : Fragment() {

    private val args by navArgs<DogDetailFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dog_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (activity as AppCompatActivity?)!!.supportActionBar!!.title =  getString(R.string.dog_category, args.category)
        loadImage()
    }

    private fun loadImage() {
        Picasso.get()
            .load(args.urlImage)
            .placeholder(R.drawable.ic_insert_photo_black_24dp)
            .error(R.drawable.ic_broken_image_black_24dp)
            .into(dogImageDetail)
    }

}