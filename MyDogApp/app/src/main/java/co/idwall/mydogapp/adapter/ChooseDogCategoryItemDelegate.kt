package co.idwall.mydogapp.adapter

interface ChooseDogCategoryItemDelegate {
    fun onTouchItem(category: String)
}