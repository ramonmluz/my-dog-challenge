package co.idwall.mydogapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import co.idwall.mydogapp.R
import co.idwall.mydogapp.adapter.ChooseDogCategoryAdapter
import co.idwall.mydogapp.adapter.ChooseDogCategoryItemDelegate
import co.idwall.mydogapp.databinding.FragmentChooseDogCategoryBinding
import co.idwall.mydogapp.network.StatusLoading
import co.idwall.mydogapp.viewmodel.ChooseDogCategoryViewModel
import kotlinx.android.synthetic.main.fragment_choose_dog_category.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ChooseDogCategoryFragment : Fragment(), ChooseDogCategoryItemDelegate {

    private val viewModel: ChooseDogCategoryViewModel by viewModel()
    private lateinit var adapter: ChooseDogCategoryAdapter
    private lateinit var binding: FragmentChooseDogCategoryBinding
    private var categories = mutableListOf<String>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_choose_dog_category, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initRecyclerView()
        onObserverDogCategoryResult()
        loadCategories()
    }

    private fun initRecyclerView() {
        adapter = ChooseDogCategoryAdapter(categories, this)
            chooseDogCategoryRecyclerView.layoutManager = LinearLayoutManager(activity)
            chooseDogCategoryRecyclerView.adapter = adapter
    }

    private fun onObserverDogCategoryResult() {
        viewModel.getCategoriesResult().observe(viewLifecycleOwner, Observer { statusLoading ->
            when (statusLoading) {
                is StatusLoading.Loading -> setVisibilityComponentsIndicator(isLoading =  true, isAreaError = false)
                is StatusLoading.Success -> onSuccess(statusLoading.data as List<String>)
                is StatusLoading.Error -> setVisibilityComponentsIndicator(isLoading =  false, isAreaError = true)
            }
        })
    }

    private fun loadCategories() {
        viewModel.loadCategories()
    }

    private fun onSuccess(categories: List<String>) {
        setVisibilityComponentsIndicator(isLoading = false, isAreaError = false)
        clearAdapter(categories)
        addInAdapter(categories)
    }

    private fun clearAdapter(categories: List<String>) {
        adapter.notifyItemRangeRemoved(0, categories.size)
        this.categories.clear()
    }

    private fun addInAdapter(categories: List<String>) {
        this.categories.addAll(categories)
        adapter.notifyItemInserted(categories.lastIndex)
    }
    private fun setVisibilityComponentsIndicator(isLoading: Boolean, isAreaError: Boolean ) {
        binding.isLoading = isLoading
        binding.isAreaError = isAreaError
        binding.contentChooseDogCategoryError.isAreaError = isAreaError
    }

    override fun onTouchItem(category: String) {
        val action = ChooseDogCategoryFragmentDirections.actionNavigationSearchToNavigationHome(category =  category)
        findNavController().navigate(action)
    }
}