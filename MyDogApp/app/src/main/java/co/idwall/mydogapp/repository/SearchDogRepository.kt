package co.idwall.mydogapp.repository

import co.idwall.mydogapp.model.DogCategory
import co.idwall.mydogapp.network.DogCategoryServiceApi

class SearchDogRepository(private val dogCategoryServiceApi: DogCategoryServiceApi) {
    suspend fun searchDogs(token: String, category: String): DogCategory =
        dogCategoryServiceApi.getDogs(token, category)
}