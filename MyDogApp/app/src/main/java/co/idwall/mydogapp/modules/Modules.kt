package co.idwall.mydogapp.modules

import co.idwall.mydogapp.common.PreferencesProvider
import co.idwall.mydogapp.network.DogCategoryServiceApi
import co.idwall.mydogapp.repository.ChooseDogCategoryRepository
import co.idwall.mydogapp.repository.LoginRepository
import co.idwall.mydogapp.repository.SearchDogRepository
import co.idwall.mydogapp.viewmodel.ChooseDogCategoryViewModel
import co.idwall.mydogapp.viewmodel.LoginViewModel
import co.idwall.mydogapp.viewmodel.SearchDogViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object Modules {
    val loginModule = module {
        factory { PreferencesProvider(get()) }
        single { providerLoginRepository(get(), get()) }
        viewModel { providerViewModel(get()) }
    }

    private fun providerLoginRepository(dogCategoryServiceApi: DogCategoryServiceApi, preferencesProvider: PreferencesProvider) = LoginRepository(dogCategoryServiceApi, preferencesProvider)
    private fun providerViewModel(loginRepository: LoginRepository) = LoginViewModel(loginRepository)

    val searchDogModule = module {
        single { providerSearchDogRepository(get()) }
        viewModel { providerSearchDogViewModel(get()) }
    }

    private fun providerSearchDogRepository(dogCategoryServiceApi: DogCategoryServiceApi) = SearchDogRepository(dogCategoryServiceApi)
    private fun providerSearchDogViewModel(searchDogRepository: SearchDogRepository) = SearchDogViewModel(searchDogRepository)


    val chooseDogCategoryModule = module {
        single { providerChooseDogCategoryRepository(get()) }
        viewModel { providerChooseDogCategoryViewModel(get()) }
    }

    private fun providerChooseDogCategoryRepository(dogCategoryServiceApi: DogCategoryServiceApi) = ChooseDogCategoryRepository(dogCategoryServiceApi)
    private fun providerChooseDogCategoryViewModel(chooseDogCategoryRepository: ChooseDogCategoryRepository) = ChooseDogCategoryViewModel(chooseDogCategoryRepository)



}
