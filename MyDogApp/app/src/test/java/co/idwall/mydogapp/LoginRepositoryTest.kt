package co.idwall.mydogapp

import co.idwall.mydogapp.common.PreferencesProvider
import co.idwall.mydogapp.model.User
import co.idwall.mydogapp.model.UserResponse
import co.idwall.mydogapp.network.DogCategoryServiceApi
import co.idwall.mydogapp.repository.LoginRepository
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class LoginRepositoryTest {

    private var dogCategoryServiceApi = mock<DogCategoryServiceApi>()
    private var preferencesProvider = mock<PreferencesProvider>()
    private lateinit var loginRepository: LoginRepository

    @Before
    fun setup() {
        this.loginRepository = LoginRepository(dogCategoryServiceApi, preferencesProvider)
    }

    @Test
    fun `Check if the user is obtained`() {

        val params = hashMapOf((Pair("email", getEmail())))

        whenever(runBlocking { dogCategoryServiceApi.getUser(params) }).thenReturn(getUserResponse())
        val userResponse = runBlocking { loginRepository.loadUser(getEmail()) }
        Assert.assertEquals(userResponse.user, getUserResponse().user)
    }

    private fun getUserResponse() = UserResponse(User(getEmail(), getToken()))

    private fun getEmail(): String {
        return "teste@teste.com"
    }

    @Test
    fun `A token should be returned`() {
        whenever(preferencesProvider.getValueAtPreference()).thenReturn(getToken())
        val token = loginRepository.getTokenAtPreference()
        Assert.assertEquals(token,getToken())
    }

    private fun getToken() = "XYZXRRR"
}