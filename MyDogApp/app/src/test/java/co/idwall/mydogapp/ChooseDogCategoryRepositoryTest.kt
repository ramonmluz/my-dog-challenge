package co.idwall.mydogapp

import co.idwall.mydogapp.network.DogCategoryServiceApi
import co.idwall.mydogapp.repository.ChooseDogCategoryRepository
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class ChooseDogCategoryRepositoryTest {
    private var dogCategoryServiceApi = mock<DogCategoryServiceApi>()
    private lateinit var chooseDogCategoryRepository: ChooseDogCategoryRepository

    @Before
    fun setup() {
        this.chooseDogCategoryRepository = ChooseDogCategoryRepository(dogCategoryServiceApi)
    }

    @Test
    fun `Check if the list got is true`() {
        chooseDogCategoryRepository.categories = getCategories()
         val categories =  runBlocking { chooseDogCategoryRepository.loadCategories()}

        Assert.assertTrue(categories == getCategories())
    }

   private fun getCategories(): MutableList<String>  = mutableListOf<String>("husky", "hound", "pug", "vira-lata")
}