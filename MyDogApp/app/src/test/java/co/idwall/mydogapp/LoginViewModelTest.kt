package co.idwall.mydogapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import co.idwall.mydogapp.model.User
import co.idwall.mydogapp.model.UserResponse
import co.idwall.mydogapp.network.StatusLoading
import co.idwall.mydogapp.repository.LoginRepository
import co.idwall.mydogapp.viewmodel.LoginViewModel
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class LoginViewModelTest {

    @get: Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: LoginViewModel

    @Mock
    private lateinit var loginRepository: LoginRepository

    @Mock
    private lateinit var viewStateObserver: Observer<StatusLoading<User>>

    @Before
    fun setup() {

        MockitoAnnotations.initMocks(this)
        viewModel = LoginViewModel(loginRepository).apply {
            getUserResult().observeForever(viewStateObserver)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `Check if loading and success states was called `() = mainCoroutineRule.runBlockingTest {

        val userResponse = UserResponse(User(getEmail(), getToken()))
        whenever(loginRepository.loadUser(getEmail())).thenReturn(userResponse)

        viewModel.signUp()

        verify(viewStateObserver).onChanged(StatusLoading.Loading(null))

        verify(viewStateObserver).onChanged(StatusLoading.Success(userResponse.user))
    }

    private fun getEmail() = "teste@teste.com"

    private fun getToken() = "XYZXRRR"

}

