package co.idwall.mydogapp

import co.idwall.mydogapp.common.EmailValidator
import org.junit.Assert
import org.junit.Test

class EmailValidatorTest {

    @Test
    fun `Check if email is not valid when empty `(){
        Assert.assertFalse( EmailValidator.isValidEmail(""))
    }

    @Test
    fun `Check if email is not valid without @ `(){
        Assert.assertFalse( EmailValidator.isValidEmail("teste"))
    }

    @Test
    fun `Check if email is not valid incomplete `(){
        Assert.assertFalse( EmailValidator.isValidEmail("teste@gmail."))
    }

    @Test
    fun `Check if email is valid `(){
        Assert.assertTrue( EmailValidator.isValidEmail("teste@gmail.com"))
    }
}